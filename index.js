require('dotenv').config();
const express = require("express");
const path = require("path");
const hbs = require("hbs");
const passport = require("passport");
const session = require("express-session");
const mongoose = require("mongoose");
const MongoStore = require("connect-mongo");

// Requerimos el archivo de configuración de nuestra DB
require("./db.js");
const Pet = require("./models/Pet");

// Requerimos nuestro archivo de configuración
require("./passport");

// Express configuration
//Usando .env intenta tirar con la variable PORT del archivo env SI NO el 3000
const PORT = process.env.PORT || 3000;
const server = express();
const router = express.Router();

//Requerimos ficheros de autenticación
const authMiddleware = require('./middlewares/auth.middleware');

// Body
server.use(express.json());
server.use(express.urlencoded({ extended: false }));

// Enrutado
const petRoutes = require("./routes/pet.routes");
const shelterRoutes = require("./routes/shelter.routes");
// Rutas al hbs
const indexRoutes = require("./routes/index.routes");
// Ruta a la autenticación
const userRoutes = require("./routes/user.routes");

server.use(
  session({
    secret: process.env.SESSION_SECRET, // Clave que le daremos. ¡Este secreto tendremos que cambiarlo en producción!
    resave: false, // Solo guardará la sesión si hay cambios en ella.
    saveUninitialized: false, // Lo usaremos como false debido a que gestionamos nuestra sesión con Passport
    //La cookie guardará durante 1 hora el login
    cookie: {
      maxAge: 3600000, // Milisegundos de duración de nuestra cookie, en este caso será una hora.
    },
    //Le digo que guarde esa sesión en nuestra DB de manera que se mantenga iniciada
    store: MongoStore.create({
      mongoUrl: "mongodb://localhost:27017/upgrade_class",
    }),
  })
);
// Añadimos el nuevo middleware al servidor para la autenticación
server.use(passport.initialize());
// Este middlware añadirá sesiones a nuestros usuarios
server.use(passport.session());

// Views con hbs
server.set("views", path.join(__dirname, "views"));
server.set("view engine", "hbs");

// Registramos el helper (hbs) justo después de hacer .set
hbs.registerHelper("gte", (a, b, opts) => {
  if (a >= b) {
    return opts.fn(this);
  } else {
    return opts.inverse(this);
  }
});

// Registrar otro helper con una función
hbs.registerHelper("uppercase", (str) => {
  return str.toUpperCase();
});

// Enrutado

// Usamos el hbs para enrutar con /
server.use("/", indexRoutes);
//En /pets se obligará a estar autenticado usando nuestro middleware
//server.use('/pets', [authMiddleware.isAuthenticated], petRoutes);
server.use('/pets', petRoutes);
server.use("/shelters", shelterRoutes);
// Registro
server.use("/users", userRoutes);

//Conectar como carpeta estática en public y así conecte con images, stylesheet y js
server.use(express.static(path.join(__dirname, "public")));

// Crearemos un middleware para cuando no encontremos la ruta que busquemos
server.use("*", (req, res, next) => {
  const error = new Error("Route not found");
  error.status = 404;
  next(error); // Lanzamos la función next() con un error
});

// Si se lanza la función
// server.use((err, req, res, next) => {
//   return res.status(err.status || 500).json(err.message || "Unexpected error");
// });

server.use((err, req, res, next) => {
  return res.status(err.status || 500).render("error", {
    message: err.message || "Unexpected error",
    status: err.status || 500,
  });
});

server.listen(PORT, () => {
  console.log(`Server running in http://localhost:${PORT}`);
});
