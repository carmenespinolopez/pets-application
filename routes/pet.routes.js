// Archivo pet.routes.js dentro de la carpeta routes
const express = require("express");
const router = express.Router();

//Mongo model
const Pet = require("../models/Pet");

//Middleware to upload images
const fileMiddlewares = require("../middlewares/file.middleware");

router.get("/", async (req, res) => {
  try {
    const pets = await Pet.find();
    return res.status(200).render("pets", { title: "Upgrade pets", pets });
    //return res.status(200).json(pets);
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get("/id/:id", async (req, res, next) => {
  try {
    const id = req.params.id;
    const pet = await Pet.findById(id);

    if (pet) {
      return res
        .status(200)
        .render("pet", { title: "Upgrade single pet", pet: pet, id: id });
      //return res.status(200).json(pet);
    } else {
      return res.status(404).json("No pet found by this id");
    }
  } catch (err) {
    //return res.status(500).json(err);
    next(err);
  }
});

router.get("/species/:species", async (req, res) => {
  const species = req.params.species;

  try {
    const petsBySpecies = await Pet.find({ species: species });
    return res.status(200).json(petsBySpecies);
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.get("/age/:age", async (req, res) => {
  const age = req.params.age;

  try {
    const petsByAge = await Pet.find({ age: { $lt: age } });
    //lt: less than, lte: less than equal...
    return res.status(200).json(petsByAge);
  } catch (err) {
    return res.status(500).json(err);
  }
});

router.post(
  "/",
  [fileMiddlewares.upload.single('picture'), fileMiddlewares.uploadToCloudinary],
  //Modo single: subir sólo un fichero
  //El campo 'picture' será donde suba la imagen
  //Usaremos el campo req.file
  async (req, res, next) => {
    // Obtenemos el nombre de la imagen
    const picture = req.file_url || null;

    try {
      // Crearemos una instancia de mascota con los datos enviados
      const newPet = new Pet({
        name: req.body.name,
        species: req.body.species,
        age: req.body.age,
        //picture: dogPicture, // Lo utilizamos en el modelo
        picture: picture
      });

      // Guardamos la mascota en la DB
      // const createdPet = await newPet.save();
      // return res.status(200).json(createdPet);

      //Al añadir el hbs lo que haremos será esto:
      await newPet.save();
      return res.redirect("/pets");
    } catch (err) {
      // Lanzamos la función next con el error para que gestione todo Express
      next(err);
    }
  }
);

//Creación nueva mascota desde el navegador usando la vista
router.get("/new", (req, res) => {
  return res.status(200).render("new-pet");
});

router.put("/edit", async (req, res, next) => {
  try {
    const id = req.body.id;

    const updatedPet = await Pet.findByIdAndUpdate(
      id, // La id para encontrar el documento a actualizar
      //{ name: req.body.name }, // Campos que actualizaremos - sólo uno por put
      { age: req.body.age },
      { new: true } // Usando esta opción, conseguiremos el documento actualizado cuando se complete el update
    );

    return res.status(200).json(updatedPet);
  } catch (err) {
    next(err);
  }
});

//Mejor hacer put así con :id que poniéndolo en el body
router.put("/:id", async (req, res, next) => {
  try {
    const id = req.params.id;

    const updatedPet = await Pet.findByIdAndUpdate(
      id, // La id para encontrar el documento a actualizar
      { name: req.body.name }, // Campos que actualizaremos - sólo uno por put
      { new: true } // Usando esta opción, conseguiremos el documento actualizado cuando se complete el update
    );

    return res.status(200).json(updatedPet);
  } catch (err) {
    next(err);
  }
});

router.delete("/:id", async (req, res, next) => {
  try {
    const id = req.params.id;

    // No será necesaria asignar el resultado a una variable ya que vamos a eliminarlo
    await Pet.findByIdAndDelete(id);
    return res.status(200).json("Pet deleted!");
  } catch (err) {
    next(err);
  }
});

module.exports = router;
