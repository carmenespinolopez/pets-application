# Pets application

Application build with node.js and Mongodb.

## Installation
Pets-app requires [mongo-connect](https://www.npmjs.com/package/connect-mongo) v4+ to run.

- Install the dependencies and devDependencies and start the server
```sh
cd pets-app
npm i
npm run dev
```
- Open localhost:4000 
- ✨Magic ✨

## Cloud version
To see the version already deployed in the cloud, follow the URL below:
[Pets application](https://upgrade-pets.herokuapp.com/)

- Database on [Mongo Cloud](https://cloud.mongodb.com/)
- Uploads on [Cloudinary](https://cloudinary.com/) 
- Application on [Heroku](https://www.heroku.com/home)
