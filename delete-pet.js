const deleteItem = (event) => {
    console.log(event.srcElement.id);

    return fetch(`http://localhost:3000/pets/${event.srcElement.id}`, {
        method: 'DELETE',
    }).then(response => {
        alert('Mascota borrada');
        window.location.reload();
    })
};

window.onload = () => {
    const btnList = document.querySelectorAll('button');

    btnList.forEach(btn => btn.addEventListener('click', deleteItem));
};